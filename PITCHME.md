---

# My first GitPitch

Note:

* Tell about Git
* Tell about GitLab
* Enjoy GitPitch

---

## A second slide

* What
* a
* wonderful
* world!

---

```python
a = "Hello"
b = "World"
z = "!"

print " ".join((a, b)) + z
```

results in:

```
Hello World!
```

+++

Text

```
# and code
```

---

# Bye!

Note:

Don't forget to thank your parents ;-)

